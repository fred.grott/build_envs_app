import 'package:build_envs_app/constants.dart';
import 'package:flutter/material.dart';

void mainDelegate() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // ignore: prefer_single_quotes
      title: "Build env - ${Constants.WHERE_AM_I}",
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: Text('Build env - ${Constants.WHERE_AM_I}'),
          ),
          body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text('Build env - ${Constants.WHERE_AM_I}',),
                  // ignore: prefer_const_constructors
                  Divider(),
                  Text('Server 1: ${Constants.SERVER_ONE}'),
                  // ignore: prefer_const_constructors
                  Divider(),
                  Text('Server 2: ${Constants.SERVER_TWO}'),
                  // ignore: prefer_const_constructors
                  Divider(),
                  // ignore: prefer_single_quotes
                  Constants.WHERE_AM_I == 'prod'
                      // ignore: prefer_const_constructors
                      ? Text('OMG! Such a prod!')
                      // ignore: prefer_const_constructors
                      : Text("You're still testing man!"),
                ],
              ))),
      debugShowCheckedModeBanner: false,
    );
  }
}
