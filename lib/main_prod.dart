import 'package:build_envs_app/constants.dart';
import 'package:build_envs_app/main.dart';

void main(){
  Constants.setEnvironment(Environment.PROD);
  mainDelegate();
}