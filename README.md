# build_envs_app

This is modified from Ishaan Bahal's medium article 

[Build variants in Flutter for multiple backend environments](https://medium.com/meeve/build-variants-in-flutter-for-multiple-backend-environments-7e139128949b)
 
 as he had sevral things wrong concening Dart types. Folks always and i repeat always go in your sdk 
 install folder and copy that analysis_options yaml file and put in your project root as it helps 
 all the ones learnuing flutter-dart in its easier to applying code examples.
 
 ## Theory
 
 We do want things such as api keys in our git repos. So we use a singleton and some creative 
 git ignore settings to keep api keys out of the git repo.
 
 ## Operational Notes
 
 Now for the fun neat part, we only need to gitignore one single file! The constants file:
 
 ```
 enum Environment { DEV, STAGING, PROD }

// ignore: avoid_classes_with_only_static_members
class Constants {
   static Map<String, dynamic> _config;

  static void setEnvironment(Environment env) {
    switch (env) {
      case Environment.DEV:
        _config = _Config.debugConstants;
        break;
      case Environment.STAGING:
        _config = _Config.qaConstants;
        break;
      case Environment.PROD:
        _config = _Config.prodConstants;
        break;
    }
  }

  // ignore: avoid_classes_with_only_static_members, non_constant_identifier_names
  static dynamic get SERVER_ONE {
    return _config[_Config.SERVER_ONE];
  }

  // ignore: non_constant_identifier_names
  static dynamic get SERVER_TWO {
    return _config[_Config.SERVER_TWO];
  }

  // ignore: non_constant_identifier_names
 static dynamic get WHERE_AM_I {
    return _config[_Config.WHERE_AM_I];
  }
}

// ignore: avoid_classes_with_only_static_members
class _Config {
  static const String SERVER_ONE = 'SERVER_ONE';
  static const String SERVER_TWO = 'SERVER_TWO';
  static const String WHERE_AM_I = 'WHERE_AM_I';
  // per dart type recommends Object instead of dynamic
  // ignore: always_specify_types
  static  Map<String, Object> debugConstants =  {
    SERVER_ONE: 'localhost:3000/',
    SERVER_TWO: 'localhost:8080/',
    WHERE_AM_I: 'local',
  };

  // ignore: always_specify_types
  static Map<String, Object> qaConstants = {
    SERVER_ONE: 'https://staging1.example.com/',
    SERVER_TWO: 'https://staging2.example.com/',
    WHERE_AM_I: 'staging',
  };

  // ignore: always_specify_types
  static Map<String, Object> prodConstants = {
    SERVER_ONE: 'https://itsallwidgets.com/',
    SERVER_TWO: 'https://flutter.io/',
    WHERE_AM_I: 'prod'
  };
}

```

That is the general format. For those getting into Storybook type user user story BDD app boards
oen can obviously add another build variant for usercase stories.

# Credits

Ishaan Bahal came up with this in his medium article:

[Build variants in Flutter for multiple backend environments](https://medium.com/meeve/build-variants-in-flutter-for-multiple-backend-environments-7e139128949b)
 
 i just polished it up according to dart type best practices.
 
 # License
 
 BSD Clause 2 Copyright 2019 Fredirck Grott
